
/*
 * Using shift is a the key point :)
 */
function reverse( myArray ) {

	var val         = myArray.shift(),
		resultArray = [];
	
	// if myArray still have a value
	if ( myArray.length ) {
		resultArray = reverse( myArray );
	}
	
	resultArray.push( val );

	return resultArray;
}

function testReverse() {
	var users = [ 'Hello', 'World', 'Baby', 'Foo', 'Bar' ];
	
	users = reverse( users );	
	console.log( users );
}

testReverse();

