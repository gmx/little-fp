
function reduce( myArray, callback, initialVal ) {
	
	var item       = myArray.pop();
        initialVal = callback( item, initialVal );	
	
	if ( myArray.length ) {
		initialVal = reduce( myArray, callback, initialVal );	
	}		

	return initialVal;	
}

function testReduce() {
	var users = [ 'Thiha Htun', 'Myo Ma Ma', 'May Nyein Charm' ];
	
	var result = reduce( users, function(item, result) {
		return result + ', ' + item;	
	}, 'WOOOO!');			

	console.log( result );
}

testReduce();
