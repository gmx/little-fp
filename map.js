

function map( myArray, callback ) {

	// console.log( '------------' );
	// console.log( myArray );

	// apply callback to current item
	var val       = myArray.pop(),
		curVal    = callback( val );
	var	mapResult = [];
	
	// console.log( 'Applied value: ' )
	// console.log( curVal );

	// if myArray still have a value
	// apply again
	if ( myArray.length ) {
		mapResult = map( myArray, callback );
	}
	
	// console.log( mapResult, curVal, myArray.length );

	mapResult.push( curVal );
	return mapResult;
}

function testMap() {
	var users = [ 'Hello', 'World', 'Baby', 'Foo', 'Bar' ];
	
	var result = map( users, function( u ) {
		return 'Mr. ' + u;
	});

	console.log( result );
}

testMap();

